const app = require('express')()

const bodyParse = require('body-parser')
const cors = require('cors')
const jwt = require('jsonwebtoken')
const jwksClient = require('jwks-rsa');

const client = jwksClient({
    jwksUri: 'https://appleid.apple.com/auth/keys',
});

const port = 8000;
app.use(bodyParse.json())
app.use(cors())

function getAppleSignigKey(kid){
    return new Promise((resolve)=>{

    client.getSigningKey(kid, (err, key) => {
        if(err){
            resolve(null);
            return err
        }
        const signingKey = key.getPublicKey();
      resolve(signingKey)
      });
    })

}

function verifyJWT(json, publicKey){
    return new Promise((resolve)=>{

    jwt.verify(json,publicKey,(err, playload)=>{
        if(err){
            return resolve(null)
        }
        resolve(playload);
    })
})

}
app.post('/login', async (req,res)=>{

    const {provider, response} = req.body;

    if(provider ==='apple'){
        const {identityToken,user} = response;
        const json = jwt.decode(identityToken,{complete:true})
        const kid = json.header.kid
        const appleKey = await getAppleSignigKey(kid);
        if(!appleKey){
            return {
                error: "Fallo en la obtension del appleKey"
            }
        }
        const payload = await verifyJWT(identityToken,appleKey);
        if(!payload){
            return {
                error: "Fallo en la verificación identityToken o el appleKey"
            }
        }
        res.json({
            status:'ok',
            response:payload
        })
    }
   
})

app.listen(port, ()=>{
    console.log('Server ready')
})